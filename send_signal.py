import os
import signal
import sys
import time

def main():
        pid = sys.argv[1]
        signals = sys.argv[2]

        print(f"pid: {pid}")

        sig_list = signals.split(',')
        for sig in sig_list:
                temp = sig.strip("\'").strip("[").strip("]")
                signal_num = signal.Signals[temp].value
                print(f"signal number: {signal_num}")

                time.sleep(2)
                os.kill(int(pid), int(signal_num))

if __name__ == "__main__":
        main()
