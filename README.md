## Description

A janky python script that I use to send a list of signals to a process. Mainly for CTFs stuff.

You should improve this one by adding help prompts, arguments type checking, or even a better way to parse the list of Signals

## Usage
```bash
python3 send_signal.py <PID> <LIST OF SIGNALS>
```
The list of signal should be separated by comma (`,`). Do not add space between each element of the list.

## Example
```bash
hacker@embryoio_level111:~$ python3 send_signal.py 239 ['SIGUSR1','SIGUSR2','SIGABRT','SIGUSR2','SIGINT']
pid: 239
signal number: 10
signal number: 12
signal number: 6
signal number: 12
signal number: 2
```